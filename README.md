# Hangzhou with Liquidity Baking for USDtz

Produced under contract for Tezos Stable Technologies, Ltd.

This protocol changes the Liquidity Baking subsidy to USDtz (`KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9`) and includes all features of Hangzhou.

Protocol Hash: `PsCUKj6wydGtDNGPRdxasZDs4esZcVD9tf44MMiVy46FkD9q1h9`

## Details

Proto alpha forked at commit `f7eb289e56825532ac175eb986aa195149e68d11` (Hangzhou snapshot)

Generation Steps:
```
make build-deps
eval $(opam env)
make

./scripts/snapshot_alpha.sh h_011
./scripts/link_protocol.sh src/proto_011_PsCUKj6w 
make

dune exec scripts/declare-new-protocol-unit-test/main.exe -- 011 Ps9fAjk3
./scripts/update_integration_test.sh
./scripts/update_opam_test.sh

make
```

Unit Testing Steps:
```
make test
```
*Note: Flextesa synchronization tests fail; Flextesa synchronization test also fail at commit `f7eb289e56825532ac175eb986aa195149e68d11` (Hangzhou snapshot) and are not run as part of Tezos CI.*

Integration Testing:
[Snapshot Source](https://mainnet.xtz-shots.io/tezos-mainnet-1719548.rolling)
```
./scripts/link_protocol.sh src/proto_011_PsvaFXDP 
./scripts/user_activated_upgrade.sh src/proto_011_PsvaFXDP 1719550
patch -p1 < scripts/yes-node.patch
dune exec scripts/yes-wallet/yes_wallet.exe -- create minimal in /tmp/yes-wallet
make

./tezos-node snapshot import ~/tezos-mainnet-1719548.rolling --data-dir /tmp/tezos-node-mainnet
./tezos-node identity generate --data-dir /tmp/tezos-node-mainnet

test_directory=$(mktemp -d -t "tezos-node-mainnet-XXXX") && cp -r "/tmp/tezos-node-mainnet/." "$test_directory"
./tezos-node run --connections 0 --data-dir "$test_directory" --rpc-addr localhost &
./tezos-client -d /tmp/yes-wallet bake for foundation1 --minimal-timestamp
./tezos-client -d /tmp/yes-wallet bake for foundation1 --minimal-timestamp
./tezos-client -d /tmp/yes-wallet bake for foundation1 --minimal-timestamp
./tezos-client -d /tmp/yes-wallet bake for foundation1 --minimal-timestamp

curl http://localhost:8732/chains/main/blocks/head | jq  .metadata.implicit_operations_results
curl http://localhost:8732/chains/main/blocks/head/context/contracts/KT1MqgYc8doArd6tGwdQzpPhE68cLoiBmdsp/storage
curl http://localhost:8732/chains/main/blocks/head/context/contracts/KT19ixQfFh1trsNVg1QvEWUgQFDb8Uc43WMp/storage
```

---

# Tezos Octez implementation

## Introduction

Tezos is a blockchain that offers both  _consensus_ and _meta-consensus_, by which we mean that Tezos comes to consensus both about the state of its ledger, and  _also_ about how the
protocol and the nodes should adapt and upgrade.
For more information about the project, see https://tezos.com.

## Getting started

Instructions to
[install](https://tezos.gitlab.io/introduction/howtoget.html), [start
using](https://tezos.gitlab.io/introduction/howtouse.html), and
[taking part in the
consensus](https://tezos.gitlab.io/introduction/howtorun.html) are
available at https://tezos.gitlab.io/.

## The Tezos software

This repository hosts **Octez**, an implementation of the Tezos blockchain.
**Octez** provides a node, a client, a baker, an endorser, an accuser, and other tools, distributed with the Tezos economic protocols of Mainnet for convenience.

In more detail, this git repository contains:
- the source code, in directory src/
- tests (mainly system tests):
  * in a Python testing and execution framework, under tests_python/
  * in an OCaml system testing framework for Tezos called Tezt, under tezt/
- the developer documentation of the Tezos software, under docs/
- a few third-party libraries, adapted for Tezos, under vendors/

The Tezos software may run either on the nodes of
the main Tezos network (mainnet) or on [various Tezos test
networks](https://tezos.gitlab.io/introduction/test_networks.html).

The documentation for developers, including developers of the Tezos software
and developer of Tezos applications and tools, is available
online at https://tezos.gitlab.io/. This documentation is always in
sync with the master branch which may however be slightly
desynchronized with the code running on the live networks.

The source code of Octez is placed under the [MIT Open Source
License](https://opensource.org/licenses/MIT).

## Contributing

### Development workflow

All development of the Tezos code happens on
GitLab at https://gitlab.com/tezos/tezos. Merge requests
(https://gitlab.com/tezos/tezos/-/merge_requests) should usually
target the `master` branch; see [the contribution
instructions](https://tezos.gitlab.io/developer/contributing.html).

The issue tracker at https://gitlab.com/tezos/tezos/issues can be used
to report bugs and to request new simple features. The [Tezos Agora
forum](https://forum.tezosagora.org/) is another great place to
discuss the future of Tezos with the community at large.

### Development of the Tezos protocol

The core of the Tezos software that implements the economic ruleset is
called the *protocol*. Unlike the rest of the source code, updates to the
protocol must be further adopted through the [Tezos
on-chain voting
procedure](https://tezos.gitlab.io/whitedoc/voting.html). Protocol
contributors are encouraged to synchronize their contributions to
minimize the number of protocol proposals that the stakeholders have
to study and to maximize the throughput of the voting procedure.

## Community

Links to community websites are gathered in the following community portals:
- https://www.tezos.help/
- https://developers.tezos.com/ (for developers of applications built on Tezos)
